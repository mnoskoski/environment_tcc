create database tcc;
create user 'tcc'@'localhost' IDENTIFIED BY '12345678';
GRANT ALL PRIVILEGES ON tcc.* TO 'tcc'@'localhost';
FLUSH PRIVILEGES;


ALTER USER 'tcc’@‘localhost’ IDENTIFIED WITH mysql_native_password BY ‘password’;
GRANT ALL PRIVILEGES ON tcc.* TO 'root'@'localhost';

CREATE TABLE `user` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(32) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;