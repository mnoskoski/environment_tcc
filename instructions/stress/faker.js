/* eslint-disable no-param-reassign */
const Faker = require('faker');
const uuid = require('uuid/v4');
const moment = require('moment');

function tedRandomDate(userContext, events, done) {
  const name = `${Faker.name.firstName()} ${Faker.name.lastName()}`;
  const senderDocument = '80659720000100';
  const recipientDocument = '39199649004';

  userContext.vars.transactionId = uuid();
  userContext.vars.timestamp = moment().toISOString();

  userContext.vars.sender = {
    senderDocument,
    name,
    /*accountNumber: '1222',*/
    accountNumber: '7640030'
  };

  userContext.vars.recipient = {
    recipientDocument,
    name,
    bankCode: '033',
    branch: '1233',
    accountNumber: Faker.finance.account(),
    accountType: 'CC',
  };

  userContext.vars.operation = {
    /*value: Faker.finance.amount(0, 25),*/
    value: 10,
    purposeCode: '10',
  };

  userContext.vars.callbackUrl = 'http://supermock.demo.sensedia.com';

  return done();
}

module.exports = {
  tedRandomDate,
};
