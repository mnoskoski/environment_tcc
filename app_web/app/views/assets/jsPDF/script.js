var today = new Date();
var date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
var dateTime = date + ' ' + time;

// console.log(dateTime)
var tableControl = localStorage.getItem('tableControl')
var dataPespControl = localStorage.getItem('dataPespControl')

var tablePrevio = localStorage.getItem('tablePrevio')
var dataPesPrevio = localStorage.getItem('dataPespPrevio')

$(document).ready(function () {

    $('#content-control').html(tableControl)
    $('#table-control tr:last').after(dataPespControl)
    $('#table-control tr:first').before(
        `
                <tr>      
                    <td class="bold" align="center" colspan="10">  TRANSAÇÃO PESP 500 - OPÇÃO 05 ART. 1. - INORMAÇÕES PARA CONTROLE MONETÁRIO</td>
                </tr>
                `
    )
    $('#content-control:first').before(
        `
                <div class="title-registers bold">
                            RELATÓRIO PESP - ${dateTime}
                        </div>
                `
    )


    $('#content-previo').html(tablePrevio)
    $('#table tr:last').after(dataPesPrevio)

    $('#table tr:first').before(
        `
                <tr>      
                    <td class="bold" align="center" colspan="10">TRANSAÇÃO PESP 500 - OPÇÃO 06 ART. 2. - INFORMAÇÕES RELATIVAS CDB/RDB E DEP. AVISO PRÉVIO</td>
                </tr>
                `
    )

    amountPrevio = localStorage.getItem('amountPrevio')
    amountControl = localStorage.getItem('amountControl')

    for (i = 0; i < amountControl; i++) {

        /* report Control */ 
        $('#saldo_atual_control'+i).val(localStorage.getItem('saldo_atual_control'+i))

    }

    for (i = 0; i < amountPrevio; i++) {

        /* report Previo */ 
        $('#saldo_anterior'+i).val(localStorage.getItem('saldo_anterior'+i))
        $('#resgates'+i).val(localStorage.getItem('resgates'+i))
        $('#emissoes'+i).val(localStorage.getItem('emissoes'+i))
        $('#taxa_media'+i).val(localStorage.getItem('taxa_media'+i))
        $('#saldo_atual'+i).val(localStorage.getItem('saldo_atual'+i))
    }
    window.print()
    // console.log(tableControl, dataPespControl)
    // console.log(tablePrevio, dataPesPrevio)
})


// var doc = new jsPDF();
// var specialElementHandlers = {
//     '#editor': function (element, renderer) {
//         return true;
//     }
// };



// $('#cmd').click(function () {   
//     doc.fromHTML($('#content').html(), 10, 10, {
//         'width': 170,
//             'elementHandlers': specialElementHandlers
//     });
//     doc.setFontSize(10);
//     doc.save('sample-file.pdf');
// });