<?php
$session = new Session();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="../views/assets/scripts/jquery-3.3.1.min.js"></script>

    <link rel="stylesheet" href="../views/assets/css/4.2.1/bootstrap.min.css">
    <script src="../views/assets/scripts/4.2.1/bootstrap.min.js"></script>
    <script src="../views/assets/scripts/4.0.0/bootstrap.min.js"></script>
    <script src="../views/assets/scripts/jquery.maskMoney.min.js"></script>
    <link rel="shortcut icon" href="../views/assets/images/favicon.png" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <link rel='stylesheet' href='../views/assets/css/animate/3.5.2/animate.min.css'>

    <link rel="stylesheet" href="../views/assets/css/preloader.css">
    <link rel="stylesheet" href="../views/app/app.css">
    <script type="text/javascript" src="../views/assets/scripts/preloader.js"></script>
    <script type="text/javascript" src="../views/app/app.js"></script>

    <!-- CHART -->
    <script src="../views/assets/scripts/Chart.min.js"></script>
    <script src="../views/assets/scripts/utils.js"></script>
    <!-- END CHART -->
    <title> Banco - Topázio </title>

</head>

<body>

    <div id="preloader">
        <div class="inner">
            <div class="lds-css ng-scope">
                <div style="width:100%;height:100%" class="lds-eclipse">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="background">

        <div class="wrapper">
        <img src="../views/assets/images/logo.png" border="0" width="15%">
                
            <!-- notifycation -->
            <div aria-live="polite" aria-atomic="true">
                <div class="toast" style="padding: 8px; position: absolute; top: 18%; right: 17%; float: right;">
                    <div class="toast-header">
                        <img class="icon-login" src="../views/assets/images/profiles/<?= $_SESSION['PHOTO'] ?>" class="rounded mr-2" alt="...">
                        <br />
                        <strong class="mr-auto"> &nbsp;&nbsp; <?= $_SESSION['NAME'] ?></strong>
                        <small>0 mins ago</small>
                        <button type="button" class="ml-2 mb-1 close" data-autohide="false" data-delay="10000" data-dismiss="toast" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="toast-body">
                        Logado como <?= $_SESSION['NAME'] ?>
                    </div>
                </div>
            </div>
            <!-- end notifycation -->
            <div class="top">

                
                <div id="file" class="file">
                    <i class="fas fa-file-excel fa-3x  gray"></i>
                    <br />
                    <div class="spinner-grow text-warning" style="width: 5rem; height: 5rem;" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <br />
                    Importando arquivo aguarde...
                </div>

            </div>

            <div class="footer">
                <div class="buttons">
                    <button id="read-file" type="button" class="btn btn-info width-buttons" data-toggle="modal" data-target="#development">
                        <i class="fas fa-eye "></i> Processar Arquivo
                    </button>
                    <!-- <button id="bt-dashboard" onclick="location.href='#'" type="button" class="btn btn-warning width-buttons" data-toggle="modal">
                        <i class="fas fa-eye "></i> Visualizar Transações
                    </button> -->
                </div>
            </div>
            <div class="footer-back">
                <button id="desconected" onclick="location.href='logof/?action=logof'" type="button" class="btn btn-danger" data-dismiss="modal">
                    <i class="fas fa-sign-out-alt fa-rotate-180"></i> Desconectar-se
                </button>
                <!-- <button onclick="location.href='../../../oracle/'" type="button" class="btn btn-danger" data-dismiss="modal">Desconectar-se</button> -->
                <!-- <a href='#'><i id="arrow-back"  class="fas fa-arrow-left fa-3x"></i></a> -->
                <!-- <a href='logof/?action=logof'>
                    <i class="fas fa-sign-out-alt fa-2x fa-rotate-180"></i>
                </a> -->
            </div>
        </div>


        <!-- Modal -->

        <div class="modal fade" id="development" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog bounceIn animated" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div id="date-modal" class="title-registers bold">
                            RELATÓRIO PESP - 20/08/2019
                        </div>
                        <div class="container-registers">
                            <div class="title bold">
                                TRANSAÇÃO PESP 500 - OPÇÃO 05 ART. 1. - INORMAÇÕES PARA CONTROLE MONETÁRIO
                            </div>

                            <div class="total-registros">
                                <div id="data-control">
                                </div>
                            </div>

                            <div class="title bold" style="margin-top: 20px;">
                                TRANSAÇÃO PESP 500 - OPÇÃO 06 ART. 2. - INFORMAÇÕES RELATIVAS CDB/RDB E DEP. AVISO PRÉVIO
                            </div>

                            <div class="total-registros">
                                <div id="data-pesp">
                                </div>
                            </div>
                        </div>

                        <!-- <div class="container-registers">
                            
                        </div> -->
                    </div>
                    <div class="modal-footer">
                        <button id="create-table" onclick="printForm()" type="button" class="btn btn-danger" data-dismiss="modal">
                            <i class="fas fa-print"></i> Imprimir Modificações
                        </button>
                        <!-- <input  type="submit" class="btn btn-info" value="Armazenar Arquivo"/> -->
                        <button id="fechar" type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal true-->
        <div class="modal fade" id="true" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered bounce animated" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-registers">
                            <i class="far fa-check-circle fa-3x true"></i>

                            <h3><span class="true font20">&nbsp; &nbsp;O Arquivo foi gerado com sucesso!!</span></h3>
                        </div>
                        <div class="modal-footer">
                            <!-- <button type="button" onclick="location.href='../../dashboard/'" class="btn btn-warning" data-dismiss="modal"><i class="fas fa-eye "></i> Visualizar Transações</button> -->
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END MODAL true-->

        <!-- Modal true-->
        <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered wobble  animated" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-registers">
                            <i class="fas fa-times fa-3x error"></i>
                            <br /> <br />
                            <h3 class="error">&nbsp; Erro ao Imprimir modificações</h3>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- END MODAL true-->

        <!-- Modal error -file-->
        <div class="modal fade" id="error-read-file" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div style="width: 65%" class="modal-dialog modal-dialog-centered wobble  animated" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-registers">
                            <i class="far fa-check-circle fa-3x true"></i>
                            <br /> <br />
                            <h3 class="true">&nbsp; O Arquivo já foi Processado...</h3>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="getReport()"class="btn btn-secondary" data-dismiss="modal">Visualizar Relatórios</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
        <!-- END MODAL error-->
    </div>

</body>

</html>