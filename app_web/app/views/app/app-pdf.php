<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <meta http-equiv="refresh" content="10"> -->
    <title></title>
    <link rel="stylesheet" href="../../views/assets/css/4.2.1/bootstrap.min.css">
    <script src="../../views/assets/scripts/jquery-3.3.1.min.js"></script>
    <script src="../../views/assets/scripts/4.2.1/bootstrap.min.js"></script>
    <script src="../../views/assets/scripts/4.0.0/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../views/assets/jsPDF/style.css">
    <link rel="stylesheet" href="../../views/app/app.css">
    <script type="text/javascript" src="../../views/app/app.js"></script>
    <script src="../../views/assets/jsPDF/script.js"></script>
    <style>
        html,
        body {
            background: #FFF;
            background-size: cover;
            display: flex;
            flex: 1;
            width: 100%;
            height: 100%;
            justify-content: center;
            align-items: center;
            font-size: 0.9em;

        }

        .title-registers {
            width: 100%;
        }

        #content-control {
            display: flex;
            flex: 2;
            flex-direction: column;
            padding: 1%;
            /* border: 1px dashed #ccc; */
            justify-content: center;
        }

        #content-previo {
            justify-content: center;
            display: flex;
            flex: 2;
            flex-direction: column;
            padding: 1%;
            /* border: 1px dashed #ccc; */
            margin-top: 10px;
        }


        .wrapper {
            display: flex;
            margin: 0 auto;
            border: 1px solid #FFF;
            width: 100%;
            height: 100%;
            border-radius: 5px;
            text-align: center;
            justify-content: center;
            background: #FFF;
            flex-direction: column;
            /* box-shadow: 0px 1px 4px #787D77; */
            padding: 2% 0% 2% 0%;
        }

        .footer-pdf {
            display: flex;
            flex: 1;
            flex-direction: column;
            padding: 1%;
            /* border: 1px dashed #ccc; */
            text-align: right;

        }
    </style>
   
</head>

<body>
    <div class="wrapper">
        <div id="content-control">
        </div>
        <div id="content-previo">
        </div>
        <div class="footer-pdf">
            Feito Por:______________________________<br /><br />
            Conferido Por: _______________________________<br />

        </div>
    </div>
    <!-- <div id="editor"></div> -->
    <!-- <button  id="cmd">Generate PDF</button> -->
    <!-- <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script> -->
    
</body>

</html>