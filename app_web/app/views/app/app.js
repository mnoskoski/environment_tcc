let amountControl = 0
let amountPrevio = 0

let config = {
    //endpoint 
    // http://saturnohmg.btfinanceira.com.br/pesp/
    // http://saturnohmg.btfinanceira.com.br/pesp/
    server: localStorage.getItem('server')

    }


$(document).ready(function () {

    loadEndPoint()

    notifycationLogin()
    $('#read-file').prop('disabled', true)
    importFile()

    $('#read-file').on('click', e => {
        $('#read-file').prop('disabled', true)
        $('#data-pesp').html('')
        $('#read-file').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Processando...')
        loadFile()
    })


    $('#fechar').on('click', e => {
        $('#read-file').prop('disabled', false)
        $('#read-file').html('<i class="fas fa-eye "></i> Visualizar Arquivo')
    })

    /* Button Transitions*/
    $('#desconected').on('click', e => {
        buttonDisabled('desconected')
    })

    /*end Button Transitions*/
})

function loadEndPoint() {

    $.getJSON('action/?action=server')

        .done(function (server) {

            if (server.PESP_SERVER == '$PESP_SERVER') {
                localStorage.setItem('server', 'http://127.0.0.1:8701/') 
                console.log(server.PESP_SERVER + ' => LOCAL')
            } else {
                localStorage.setItem('server', server.PESP_SERVER)
                console.log(server.PESP_SERVER+ ' => REMOTE');

            }
        })
        .fail(function (server) {
            console.log(server)
        })

}
function importFile() {
    
    setTimeout(() => {
        $('#file').html(`
            <i class="fas fa-file-excel fa-3x  true"></i>
        `+ '<br/><br/>PESP_' + getDate() + '.XLSX').fadeOut(1000)
        $('#file').html(`
            <i class="fas fa-file-excel fa-3x  true"></i>
        `+ '<br/><br/>PESP_' + getDate() + '.XLSX').fadeIn(1000)
        $('#read-file').prop('disabled', false)
    }, 5000);

}
function loadFile() {

    $.getJSON(config.server + 'procs')

        .done(function (data) {
            console.log(data)
            getPespControl()
            getPespPrevio()
        })
        .fail(function (error) {
            console.log(error)
            showModal('error-read-file')
            buttonEnable('read-file')
        })

}
function getReport() {
    console.log('getReport');
    buttonDisabled('read-file')
    getPespControl()
    getPespPrevio()
}
function getPespControl() {

    $.getJSON(config.server + 'controls')

        .done(function (data) {

            console.log(data.data)

            var fields = Array()
            $('#date-modal').html('RELATÓRIO PESP ' + getDate())
            var tableControl = ` 
                <table id="table-control" width="100%">
                    <tr>
                        <td class="bold" align="center">DESCRICAO COSIF</td>
                        <td class="bold" align="center">CODIGO BCE</td>
                        <td class="bold" align="center">SALDO ATUAL</td>
                    </tr>
                </table>
            `;

            for (i = 0; i < data.data.length; i++) {

                fields.push(`
                    <tr>
                        <td align="left">${data.data[i].DESCRICAO}</td>
                        <td align="center">${data.data[i].CODIGO_BCE}</td>
                        <td align="center"><input type="text" name="saldo_atual_control" id="saldo_atual_control${[i]}" value="${data.data[i].SALDO_ATUAL}"></td>
                    </tr>
                `)
            }



            $('#data-control').html(tableControl)
            $('#table-control tr:last').after(fields)

            amountControl = data.data.length

            for (n = 0; n < amountControl; n++) {

                $('#saldo_atual_control' + n).val(formatMoneyBRL($('#saldo_atual_control' + n).val()))
            }

            setFormControl(tableControl, fields)

            showModal('development')

        })
        .fail(function (error) {
            console.log(error)

        })
}
function getPespPrevio() {

    $.getJSON(config.server + 'pesps')

        .done(function (dataPesp) {

            var fieldsPesp = new Array()

            console.log(dataPesp);

            var tablePrevio = `
                <table id="table" width="100%">
                    <tr>
                        <td class="bold" align="center">GRUPO FINANCEIRO</td>
                        <td class="bold" align="center">TIPO</td>
                        <td class="bold" align="center">SALDO ANTERIOR</td>
                        <td class="bold" align="center">RESGATES</td>
                        <td class="bold" align="center">EMISSÕES</td>
                        <td class="bold"align="center">TAXA MÉDIA</td>
                        <td class="bold" align="center">SALDO ATUAL</td>
                    </tr>
                  </table>
            `;

            for (i = 0; i < dataPesp.data.length; i++) {

                fieldsPesp.push(`
                    <tr>
                        <td align="left">${dataPesp.data[i].TIPO}</td>
                        <td align="left">${dataPesp.data[i].IND_PAPEL}</td>
                        <td align="left"><input type="text" name="saldo_anterior" id="saldo_anterior${[i]}" value="${dataPesp.data[i].SALDO_ANTERIOR}"></td>
                        <td align="left"><input type="text" name="resgates" id="resgates${[i]}" value="${dataPesp.data[i].RESGATES}"></td>
                        <td align="left"><input type="text" name="emissoes" id="emissoes${[i]}" value="${dataPesp.data[i].EMISSOES}"></td>
                        <td align="left"><input type="text" name="taxa_media" id="taxa_media${[i]}" value="${dataPesp.data[i].TAXA_MEDIA}"></td>
                        <td align="left"><input type="text" name="saldo_atual" id="saldo_atual${[i]}" value="${dataPesp.data[i].SALDO_ATUAL}"></td>                 
                    </tr>   
                `)


            }

            $('#data-pesp').html(tablePrevio)
            $('#table tr:last').after(fieldsPesp)

            amountPrevio = dataPesp.data.length
            setFormPrevio(tablePrevio, fieldsPesp)

            /* FORMAT NUMBER BRL*/
            for (n = 0; n < amountPrevio; n++) {

                $('#saldo_anterior' + n).val(formatMoneyBRL($('#saldo_anterior' + n).val()))
                $('#resgates' + n).val(formatMoneyBRL($('#resgates' + n).val()))
                $('#emissoes' + n).val(formatMoneyBRL($('#emissoes' + n).val()))
                $('#saldo_atual' + n).val(formatMoneyBRL($('#saldo_atual' + n).val()))
            }

            // showModal('development')
        })
        .fail(function (error) {
            console.log(error)
            // alert('Erro em desenvolvimento...')
        })
}

function formatMoneyBRL(n, c, d, t) {
    c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}
function printForm() {

    urlPush = 'pdf/'

    setInputControl()
    setInputsPrevio()
    buttonEnable('read-file')

    window.open(urlPush, '_BLANK')

}

function setFormControl(tableControl, dataControl) {

    localStorage.setItem('amountControl', amountControl)
    localStorage.setItem('tableControl', tableControl)
    localStorage.setItem('dataPespControl', dataControl)

    console.log(localStorage.getItem('amountControl'))

}

function setFormPrevio(tablePrevio, dataPrevio) {

    localStorage.setItem('amountPrevio', amountPrevio)
    localStorage.setItem('tablePrevio', tablePrevio)
    localStorage.setItem('dataPespPrevio', dataPrevio)

    // console.log(localStorage.getItem('dataPespPrevio'))

}
function setInputControl() {

    for (i = 0; i < amountControl; i++) {

        localStorage.setItem('saldo_atual_control' + i, $('#saldo_atual_control' + i).val())
        console.log(localStorage.getItem('saldo_atual_control' + i))
    }
}
function setInputsPrevio() {

    for (i = 0; i < amountPrevio; i++) {

        localStorage.setItem('saldo_anterior' + i, $('#saldo_anterior' + i).val())
        localStorage.setItem('resgates' + i, $('#resgates' + i).val())
        localStorage.setItem('emissoes' + i, $('#emissoes' + i).val())
        localStorage.setItem('taxa_media' + i, $('#taxa_media' + i).val())
        localStorage.setItem('saldo_atual' + i, $('#saldo_atual' + i).val())

        // console.log(localStorage.getItem('saldo_anterior' + i))
        // console.log(localStorage.getItem('resgates' + i))
        // console.log(localStorage.getItem('emissoes' + i))
        // console.log(localStorage.getItem('taxa_media' + i))
        // console.log(localStorage.getItem('saldo_atual' + i))
    }

    // console.log(localStorage.getItem('saldo_anterior0'))
}

function setScroll() {

    // var scrollHeight = $(".modal")[0].scrollHeight

    $(".modal").animate({
        scrollTop: 750000
    }, 30000)


}

function getDate(format) {

    var date = new Date();

    var today = {
        'year': date.getFullYear(),
        'month': (date.getMonth() + 1) > 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1),
        'day': date.getDate()
    }

    return today.year + '0' + today.month + '' + today.day
}

function buttonDisabled(div) {
    $('#' + div).prop('disabled', true)
    $('#' + div).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Processando...')
    $('#preloader').delay(350).fadeOut('slow')
}
function buttonEnable(div) {
    $('#' + div).prop('disabled', false)
    $('#' + div).html('<i class="fas fa-eye "></i> Processar Arquivo')
}

function showModal(modal) {
    $(document).ready(function () {
        $('#' + modal).modal('show')
    })
}
function closeModal(modal) {
    $(document).ready(function () {
        $('#' + modal).modal('hide')
    })
}

function randomChart() {
    console.log('robot => chart')
    setInterval(function () {
        $('#randomData').click()
    }, 4000)
}

function notifycationLogin() {

    $(".toast").toast({ autohide: false })
    $('.toast').toast('show')

    setTimeout(function () {
        $('.toast').toast('hide')

    }, 10000)
}