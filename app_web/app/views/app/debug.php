<?php
$session = new session();
$file = new FileDebug();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../../views/assets/css/preloader.css">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Debug - Saque Pague</title>

    <style>
        html,
        body {
            display: flex;
            flex: 1;
            background: #ddd;
            background-size: cover;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            flex-direction: column;
            height: 100%;
            width: 100%;
            font-size: 0.8em;
            border: 0px dashed #000;
        }

        .wrapper {
            /* display: flex; */
            flex: 1;
            justify-content: center;
            width: 80%;
            align-items: center;
            border: 1px solid #ddd;
            margin: 0 auto;
            background: #FFF;
            border-radius: 5px;
            flex-direction: column;
            padding: 2%;

        }

        .btn-info {
            font-size: 1.9em !important;
            width: 20% !important;
        }

        .success {
            color: green;
        }

        .danger {
            color: red;
        }

        .info {
            color: #17a2b8;
        }
        .header{
            text-align: center;
            font-size: 2.1em;
            font-weight: 500;
        }
        .file{
            font-size: 1.7em;
            font-weight: 500;
        }
    </style>
</head>

<body>
    <div id="preloader">
        <div class="inner">
            <div class="lds-css ng-scope">
                <div style="width:100%;height:100%" class="lds-eclipse">
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <span class='header'>Debugador Arquivo de Conciliação <span class="info">[ <?=$file->nameFile(0)?> ]</span></span>
        <hr>
        <button id="file-loader" type="button" class="btn btn-info">Reloader</button>
        <hr>
        <div class="file" id="file">
            <?=$file->readFile()?>
        </div>
    </div>

    <script>
        $(window).on('load', function() {
            $('#preloader .inner').fadeOut();
            $('#preloader').delay(350).fadeOut('slow')
            $('body').delay(350).css({
                'overflow': 'visible'
            })
        })
        $('#file-loader').on('click', e => {
            $('#file-loader').prop('disabled', true)
            $('#file-loader').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Processando...')
            window.location.reload()
        })
    </script>
</body>

</html>