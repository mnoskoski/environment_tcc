<?php

if (isset($_GET['action'])) {

    switch ($_GET['action']) {

        case 'logof':
            $session = new Session();
            $session->logof();
            break;

        case 'filedebug':
            include "debug.php";
            break;

        case 'json':
            include 'json.php';
            break;

        case 'showfile':
            $file = new File();
            $dataFile = $file->showFile();
            print json_encode($dataFile);
            break;

        case 'server':
            // $_ENV = parse_ini_file('/var/www/html/.env');
            // $_ENV = parse_ini_file('views/app/.env');
            $_ENV = array('PESP_SERVER'=>'http://saturnohmg.btfinanceira.com.br/pesp/');
            print json_encode($_ENV);
            break;
    }
}
