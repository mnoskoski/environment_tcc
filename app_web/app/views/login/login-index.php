<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../views/login/login.css">
    <link rel="stylesheet" href="../views/login/preloader.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="../views/login/login.js"></script>

    <link rel="shortcut icon" href="../views/assets/images/favicon.png"/>

    <meta name="msapplication-TileImage" content="https://www.bancotopazio.com.br/wp-content/themes/topazio-style/img/favicon/ms-icon-144x144.png">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css'>


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <title>Banco - Topazio</title>
</head>

<body>
    <div id="preloader">
        <div class="inner">
            <div class="lds-css ng-scope">
                <div style="width:100%;height:100%" class="lds-eclipse">
                    <div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="panel">

            <div class="logo">
               <!-- Logo Banco Topazio -->
               <div class="logo-topazio">
                   <img src="../views/assets/images/logo.png" border="0" width=70%">
                </div>
            </div>

            <div class="login">
                <div class="login-left">
                <i class="fas fa-user"></i>
                </div>
                <div class="login-right">
                    <div class="input"><input  type="text" name="login" id="login" class="form-login" ></div>
                </div>
            </div>
            <div class="password">
                <div class="login-left">
                <i class="fas fa-lock"></i>
                </div>
                <div class="login-right">
                    <div class="input">
                        <input type="password"  name="password" id="password" class="form-login" ></div>
                </div>
            </div>

            <div class="footer-login">
                <button type="submit" id="button-login" class="btn btn-info " data-toggle="modal">Conectar-se</button>
            </div>
            <div class="footer-login">
                <small>SATURN - V-1.0.4</small>
            </div>
            <!-- <div class="footer-login">
                
                <a href='#'><i onclick="location.href='../../../'" class="fas fa-arrow-left fa-lg"></i></a>
            </div> -->
        
            
        </div>
        <div class="footer">
            <div class="text-footer"> 
            <!-- <i class="far fa-file-excel"></i> -->
                <!-- &nbsp;&nbsp;PESP</div> -->
                PESP - Relatórios &nbsp;&nbsp;<img src="../views/assets/images/favicon.png" border="0" width="3%" height="3%">
        </div>
        
    </div>
    <!-- MODAL -->

    <!-- Modal -->
    <div style="width:100%" class="modal fade" id="panel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Alerta de Autenticação...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
             
                    Login efetuado com sucesso!!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

    </div>
    <!-- END MODAL -->

    <!-- Modal -->
    <div style="width:100%" class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered wobble  animated" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Alerta de Autenticação...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <i class="fas fa-times fa-lg red"></i> Login ou Senha Incorretos!!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

    </div>
    <!-- END MODAL -->

    <!-- Modal -->
    <div style="width:100%" class="modal fade" id="Alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered wobble  animated" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Alerta de Validação...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                 
                    <i class="fas fa-times fa-lg red"></i> Preencha todos os campos!!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>

    </div>
    <!-- END MODAL -->

</body>

</html>