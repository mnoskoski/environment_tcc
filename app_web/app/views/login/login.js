$(window).on('load', function () {
    $('#preloader .inner').fadeOut();
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow': 'visible'});
})

/* CONFIG */

var logo = '../views/assets/images/logo.png';
const url = 'auth/';

/* END CONFIG */

$(document).ready(function () {
    submit()
    
    $('#login').focus()
    
    var x = setTimeout(function(){
        
        $('.logo-topazio').html('<img src="'+logo+'" width=70%"/>').fadeOut(900)
        $('.logo-topazio').html('<img src="'+logo+'" width=70%"/>').fadeIn(3000)

    },2000)

    

    setInterval(function(){
        
        clearTimeout(x)
        
        $('.logo-topazio').html('<img src="'+logo+'" width=70%"/>').fadeOut(900)
        $('.logo-topazio').html('<img src="'+logo+'" width=70%"/>').fadeIn(3000)

    },10000)

    $('#button-login').on('click', e => {

        let login = $('#login').val()
        let password = $('#password').val()

        if (login == '' || password == '') {

            showModal('Alert')

        } else {
            buttonDisabled()
            $.post({
                method: "POST",
                url: url,
                data: { login: login, password: password }
            })
                .done(function (msg) {
                   console.log(msg)
                    var auth = JSON.parse(msg)
                    if (auth.success == true) {
                        document.location = '../app/'
                        console.log(auth)
                    } else {
                        showModal('error')
                        buttonEnable()
                        console.log(auth)

                    }

                })

        }

    })

})
function buttonDisabled() {
    $('#button-login').prop('disabled', true)
    $('#button-login').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Processando...')
}
function buttonEnable() {
    $('#button-login').prop('disabled', false)
    $('#button-login').html('Conectar-se')
}
function submit() {

    $(document).on('keyup', e => {
        if (e.keyCode == 13) {
            $('#button-login').click()
            buttonDisabled()
        }
    })
}
function showModal(modal) {
    $(document).ready(function () {
        $('#' + modal).modal('show')
    })
}
