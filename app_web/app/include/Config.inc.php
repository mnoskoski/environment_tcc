<?php

function autoLoader($Class){

    $dirName = 'model';

    if(file_exists("{$dirName}/{$Class}.class.php")){

        require_once("{$dirName}/{$Class}.class.php");

    }else{

        print "NO FILE: {$dirName}/{$Class}<br/>";
        
    }
}

spl_autoload_register("autoLoader");

?>