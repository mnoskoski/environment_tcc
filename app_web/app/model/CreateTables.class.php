<?
require_once "../model/ConnectionORACLE.class.php";
require_once "../model/Query.class.php";

class createTables extends Connection{

    public function createTable($table){

        /* Tratar se Tabela já existe */

        $createTable = 
        '
        CREATE TABLE '.$table.'( 
            ID numeric(11) NOT NULL,
            IDENTIFICATOR varchar2(300) NOT NULL UNIQUE,
            PRIMARY KEY (ID)
          )
        ';
        $stid = oci_parse($this->ConnectionORACLE(), $createTable);
        
        $tb = oci_execute($stid);

        if($tb){print 1;}else{print 'ERROR CREATE TABLE';}
    }
    
}

$query = new Query;
$count = $query->countTables('SEP_172_20190429');

if($count == 1){
    // print $count;
    // exit();
}else{
    $createTable = new createTables;
    $createTable->createTable('SEP_172_20190429');
}


?>