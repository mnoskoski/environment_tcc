<?php

// require_once "../model/ConnectionORACLE.class.php";
// require_once "../model/ConnectionOracleLocal.class.php";

class Query extends ConnectionORACLE{

    public function countTables($table){
        $array = array();
        $sql = ('SELECT COUNT(*) AS AMOUNT  FROM ALL_TABLES WHERE TABLE_NAME = :TABLE_NAME');
        $stId = oci_parse($this->ConnectionORACLE(), $sql);
        oci_bind_by_name($stId, ':TABLE_NAME', $table);
        oci_execute($stId);

        while (($row = oci_fetch_array($stId, OCI_BOTH)) != false) {
            array_push($array, $row);
        }
        $count = $array[0]['AMOUNT'];
        return $count;
    }

    public function conciliationTopazio(){

        $query = ("
            SELECT
                MC.FECHAPROCESO,
                MC.FECHAVALOR,
                MC.MARCAAJUSTE,
                MC.CAPITALREALIZADO,
                MC.DEBITOCREDITO,
                MC.DESCRIPCIONHISTORICO,
                Mc.CONCEPTO
            FROM   TOPAZIO.ASIENTOS A
            INNER  JOIN TOPAZIO.MOVIMIENTOS_CONTABLES MC
            ON     (MC.FECHAPROCESO = A.FECHAPROCESO)
            WHERE  A.FECHAPROCESO = TO_DATE('17052019','DDMMYYYY')
            AND    MC.ASIENTO = A.ASIENTO
            AND    A.ESTADO = 77
            AND    A.OPERACION = 9003
            AND    ROWNUM <= 1000
         ");
        /* Consulta: Oper9001 */

        $stId = oci_parse($this->ConnectionORACLE(), $query);
        //  oci_bind_by_name($stId,':TABLE_NAME', $table);
        oci_execute($stId);

        while (($row = oci_fetch_assoc($stId)) != false) {
            $array[] = $row;
        }

        print json_encode($array);
    }

    public function analyticTopazio($day, $month, $year){
       
        $date = $day . "-" . $month . "-" . $year;

        $query = ("
            SELECT 
            'online' MODO,
            'C' CRED_DEB,
            substr(t.DATAVALOR,-2)||substr(t.DATAVALOR,0,2) DT_PROC,
            substr(t.DATAVALOR,-2)||substr(t.DATAVALOR,0,2) DT_SEP,
            TO_NUMBER(t.NSU) NSU,
            TO_CHAR(t.NSUTPZ) NSUTPZ,
            to_char(to_number(t.codbancodep)) banco,
            to_char(to_number(t.agenciabancodep)) agencia,
            to_char(to_number(t.nrocontadep)) conta,
            t.valortransacao valor,
            nvl(t.valortarifa,0) tarifa,
            t.PROCESSDATE_P2 TPZPROC,
            to_CHAR(t.TERMINAL) TERMINAL,
            to_CHAR(t.TERMINALID) TERMINALID,
            CASE WHEN t.ESTADO = 'C' THEN
                            'SUCESSO'
                            WHEN t.ESTADO = 'A' THEN
                            'NAO PROCESSADO'
                            WHEN t.ESTADO = 'Z' THEN
                            'CANCELADO'
                            ELSE
                            'ERRO'
            END  STATUS,
            'DEPOSITO' TIPO
            FROM topazio.TPZ_BARRAMENTO t
            WHERE 
            t.PROCESSDATE_P2 >= to_date('01012019','ddmmyyyy')
            AND t.PROCESSDATE_P2 between to_date('" . $date . "','dd-mm-yyyy') AND to_date('" . $date . "','dd-mm-yyyy')                                                                                                                                                                                                         -- MES
            
            --AND  ROWNUM <= 1  

            UNION ALL
            SELECT 
            'offline' MODO,
            s.CRED_DEB,
            substr(s.id,0,4) DT_PROC,
            substr(s.id,0,4) DT_SEP,
            to_number(substr(s.id,8)) NSU,
            '' NSUTPZ,
            to_char(s.codbanco) banco,
            to_char(s.agencia),
            to_char(s.conta),
            s.valor,
            nvl(s.tarifadep,0) tarifa,
            s.DATA_AGENDAMENTO TPZPROC,
            (SELECT TO_CHAR(codatm) FROM topazio.DEPOSITOSATM_DET d WHERE d.VALOR = s.valor and substr(d.NSU,7) = substr(s.id,7) AND ROWNUM <= 1 ) TERMINAL,
            '' TERMINALID,
            CASE WHEN s.ESTADO_PROCESSO = 'P' THEN
                'SUCESSO'
                ELSE
                'ERRO'
            END STATUS,
            CASE WHEN CRED_DEB = 'C' AND  to_number(substr(s.id,8)) > 4000000 THEN
                'DEPOSITO'
                WHEN CRED_DEB = 'C' AND  to_number(substr(s.id,8)) < 4000000 THEN
                'RECARGAS'
                WHEN CRED_DEB = 'D' AND  to_number(substr(s.id,8)) < 4000000 THEN
                'SAQUES'
            END TIPO
            FROM topazio.SEP_AGENDADOR S
            WHERE
            DATA_AGENDAMENTO between to_date('" . $date . "','dd-mm-yyyy') AND to_date('" . $date . "','dd-mm-yyyy')
            
            --AND  ROWNUM <= 1   

            ORDER BY DT_PROC ASC

        ");
        $stId = oci_parse($this->ConnectionDGCRITICO(), $query);
        oci_execute($stId);

        while (($row = oci_fetch_assoc($stId)) != false) {
            $array[] = $row;
        }

       
        print json_encode($array);

    }

    public function topazHomolog(){

        $query = (
            "
            SELECT * FROM TOPAZ_HOMOLOG
            WHERE TIPO LIKE '%RECARGAS%'
            OR TIPO LIKE '%SAQUES%'
            -- ID >= 1 AND ID <= 100
            ");
        $stId = oci_parse($this->ConnectionOracleDesenv(), $query);
        oci_execute($stId);

        while (($row = oci_fetch_assoc($stId)) != false) {
            
            $array[] = $row;
        }
        print json_encode($array);
    }
}

