==================================================================================================
-- DEPOSITOS - MENSAL - COMPLETAO
==================================================================================================

SELECT 
'online' MODO,
'C' CRED_DEB,
substr(t.DATAVALOR,-2)||substr(t.DATAVALOR,0,2) DT_PROC,
substr(t.DATAVALOR,-2)||substr(t.DATAVALOR,0,2) DT_SEP,
TO_NUMBER(t.NSU) NSU,
TO_CHAR(t.NSUTPZ) NSUTPZ,
to_char(to_number(t.codbancodep)) banco,
to_char(to_number(t.agenciabancodep)) agencia,
to_char(to_number(t.nrocontadep)) conta,
t.valortransacao valor,
nvl(t.valortarifa,0) tarifa,
t.PROCESSDATE_P2 TPZPROC,
to_CHAR(t.TERMINAL) TERMINAL,
to_CHAR(t.TERMINALID) TERMINALID,
CASE WHEN t.ESTADO = 'C' THEN
                'SUCESSO'
                WHEN t.ESTADO = 'A' THEN
                'NAO PROCESSADO'
                WHEN t.ESTADO = 'Z' THEN
                'CANCELADO'
                ELSE
                'ERRO'
END  STATUS,
'DEPOSITO' TIPO
FROM topazio.TPZ_BARRAMENTO t
WHERE 
t.PROCESSDATE_P2 >= to_date('01012019','ddmmyyyy')              -- DELIMITADOR DA CONSULTA
AND t.PROCESSDATE_P2 between to_date('01/05/2019','dd/mm/yyyy') AND to_date('02/05/2019','dd/mm/yyyy')        -- FILTRO                                                                                                                                                                                                          -- MES
UNION all
SELECT 
'offline' MODO,
s.CRED_DEB,
substr(s.id,0,4) DT_PROC,
substr(s.id,0,4) DT_SEP,
to_number(substr(s.id,8)) NSU,
'' NSUTPZ,
to_char(s.codbanco) banco,
to_char(s.agencia),
to_char(s.conta),
s.valor,
nvl(s.tarifadep,0) tarifa,
s.DATA_AGENDAMENTO TPZPROC,
(SELECT TO_CHAR(codatm) FROM topazio.DEPOSITOSATM_DET d WHERE d.VALOR = s.valor and substr(d.NSU,7) = substr(s.id,7) AND ROWNUM <= 1 ) TERMINAL,
'' TERMINALID,
CASE WHEN s.ESTADO_PROCESSO = 'P' THEN
                'SUCESSO'
                ELSE
                'ERRO'
END STATUS,
CASE WHEN CRED_DEB = 'C' AND  to_number(substr(s.id,8)) > 4000000 THEN
                'DEPOSITO'
                WHEN CRED_DEB = 'C' AND  to_number(substr(s.id,8)) < 4000000 THEN
                'RECARGAS'
                WHEN CRED_DEB = 'D' AND  to_number(substr(s.id,8)) < 4000000 THEN
                'SAQUES'
END TIPO
FROM topazio.SEP_AGENDADOR S
WHERE
DATA_AGENDAMENTO between to_date('01/05/2019','dd/mm/yyyy') AND to_date('02/05/2019','dd/mm/yyyy')      -- FILTRO
-- DATA_AGENDAMENTO between TRUNC(ADD_MONTHS(SYSDATE, -1),'MM') AND trunc(sysdate, 'mm') - 1      -- FILTRO
-- APOS                                                                                                                                                                                                                             -- MES/ANO
--AND CRED_DEB = 'C' AND  to_number(substr(s.id,8)) > 4000000                             -- DEPOSITOS
--AND CRED_DEB = 'C' AND  to_number(substr(s.id,8)) < 4000000             -- RECARGAS
--AND CRED_DEB = 'D' AND  to_number(substr(s.id,8)) < 4000000             -- SAQUES
ORDER BY DT_PROC ASC;

--
SELECT EXTRACT(YEAR FROM insstamp),EXTRACT(month FROM insstamp),count(1),sum(valortransacao) FROM TPZ_BARRAMENTO WHERE ESTADO IN ('C') GROUP BY EXTRACT(YEAR FROM insstamp),EXTRACT(month FROM insstamp) ORDER BY EXTRACT(YEAR FROM insstamp),EXTRACT(month FROM insstamp) asc ;
--

--
SELECT modo,
       cred_deb,
       dt_proc,
       dt_sep,
       nsu,
       nsutpz,
       banco,
       agencia,
       conta,
       valor,
       tarifa,
       tpzproc,
       terminal,
       terminalid,
       status,
       tipo
  FROM (SELECT 'online' modo,
               'C' cred_deb,
               substr(t.datavalor, -2) || substr(t.datavalor, 0, 2) dt_proc,
               substr(t.datavalor, -2) || substr(t.datavalor, 0, 2) dt_sep,
               to_number(t.nsu) nsu,
               to_char(t.nsutpz) nsutpz,
               to_char(to_number(t.codbancodep)) banco,
               to_char(to_number(t.agenciabancodep)) agencia,
               to_char(to_number(t.nrocontadep)) conta,
               t.valortransacao valor,
               nvl(t.valortarifa, 0) tarifa,
               t.processdate_p2 tpzproc,
               to_char(t.terminal) terminal,
               to_char(t.terminalid) terminalid,
               CASE
                  WHEN t.estado = 'C' THEN
                   'SUCESSO'
                  WHEN t.estado = 'A' THEN
                   'NAO PROCESSADO'
                  WHEN t.estado = 'Z' THEN
                   'CANCELADO'
                  ELSE
                   'ERRO'
               END status,
               'DEPOSITO' tipo
          FROM topazio.tpz_barramento t
         WHERE 
           t.processdate_p2 >= to_date('01012019', 'ddmmyyyy') -- DELIMITADOR DA CONSULTA
           AND t.processdate_p2 BETWEEN to_date('26/06/2019', 'dd/mm/yyyy') AND to_date('26/06/2019', 'dd/mm/yyyy') -- FILTRO               
        
        -- MES
        UNION ALL
        
        SELECT 'offline' modo,
               s.cred_deb,
               substr(s.id, 0, 4) dt_proc,
               substr(s.id, 0, 4) dt_sep,
               to_number(substr(s.id, 8)) nsu,
               '' nsutpz,
               to_char(s.codbanco) banco,
               to_char(s.agencia),
               to_char(s.conta),
               s.valor,
               nvl(s.tarifadep, 0) tarifa,
               s.data_agendamento tpzproc,
               (SELECT to_char(codatm)
                  FROM topazio.depositosatm_det d
                 WHERE d.valor = s.valor
                   AND substr(d.nsu, 7) = substr(s.id, 7)
                   AND rownum <= 1) terminal,
               '' terminalid,
               CASE
                  WHEN s.estado_processo = 'P' THEN
                   'SUCESSO'
                  ELSE
                   'ERRO'
               END status,
               CASE
                  WHEN cred_deb = 'C' AND to_number(substr(s.id, 8)) > 4000000 THEN
                   'DEPOSITO'
                  WHEN cred_deb = 'C' AND to_number(substr(s.id, 8)) < 4000000 THEN
                   'RECARGAS'
                  WHEN cred_deb = 'D' AND to_number(substr(s.id, 8)) < 4000000 THEN
                   'SAQUES'
               END tipo
          FROM topazio.sep_agendador s
         WHERE data_agendamento BETWEEN to_date('26/06/2019', 'dd/mm/yyyy') AND to_date('26/06/2019', 'dd/mm/yyyy')
        ) x
 WHERE x.nsu = &nsu
 ORDER BY dt_proc ASC;
 --
 