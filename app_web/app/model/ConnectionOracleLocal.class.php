
<?php
class ConnectionOracleLocal{

  private $user = 'SYSTEM';
  private $password = 'j6t2h';
  private $connection_string = 'localhost/orcl';

  public function ConnectionLocal(){

    $conn = oci_connect($this->user, $this->password, $this->connection_string);

    if (!$conn) {
      $e = oci_error();
      trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    } else {
      // print 'Connected!';
      return $conn;
    }
  }

 
}
