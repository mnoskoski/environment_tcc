<?php

class Add extends ConnectionORACLE{

    public function insertTopazAPI(){

        $sql = "INSERT INTO TOPAZ_BKP (
                MODO,
                CRED_DEB,
                DT_PROC,
                DT_SEP,
                NSU,
                NSUTPZ,
                BANCO,
                AGENCIA,
                CONTA,
                VALOR,
                TARIFA,
                TPZPROC,
                TERMINAL,
                TERMINALID,
                STATUS,
                TIPO
            )VALUES(
                :MODO,
                :CRED_DEB,
                :DT_PROC,
                :DT_SEP,
                :NSU,
                :NSUTPZ,
                :BANCO,
                :AGENCIA,
                :CONTA,
                :VALOR,
                :TARIFA,
                :TPZPROC,
                :TERMINAL,
                :TERMINALID,
                :STATUS,
                :TIPO
            )";
        //AMOUNT JSON
        $amount = $_POST['amount'];
        // print $amount;
        // var_dump($_POST['data']);
        // exit();
        for ($i = 1; $i < $amount; $i++) {

            $add = oci_parse($this->ConnectionLocal(), $sql);
            oci_bind_by_name($add, "MODO", $_POST['data'][$i]['MODO']);
            oci_bind_by_name($add, "CRED_DEB", $_POST['data'][$i]['CRED_DEB']);
            oci_bind_by_name($add, "DT_PROC", $_POST['data'][$i]['DT_PROC']);
            oci_bind_by_name($add, "DT_SEP", $_POST['data'][$i]['DT_SEP']);
            oci_bind_by_name($add, "NSU", $_POST['data'][$i]['NSU']);
            oci_bind_by_name($add, "NSUTPZ", $_POST['data'][$i]['NSUTPZ']);
            oci_bind_by_name($add, "BANCO", $_POST['data'][$i]['BANCO']);
            oci_bind_by_name($add, "AGENCIA", $_POST['data'][$i]['AGENCIA']);
            oci_bind_by_name($add, "CONTA", $_POST['data'][$i]['CONTA']);
            oci_bind_by_name($add, "VALOR", $_POST['data'][$i]['VALOR']);
            oci_bind_by_name($add, "TARIFA", $_POST['data'][$i]['TARIFA']);
            oci_bind_by_name($add, "TPZPROC", $_POST['data'][$i]['TPZPROC']);
            oci_bind_by_name($add, "TERMINAL", $_POST['data'][$i]['TERMINAL']);
            oci_bind_by_name($add, "TERMINALID", $_POST['data'][$i]['TERMINALID']);
            oci_bind_by_name($add, "STATUS", $_POST['data'][$i]['STATUS']);
            oci_bind_by_name($add, "TIPO", $_POST['data'][$i]['TIPO']);

            oci_execute($add);

            if ($add) {
                print 'Inserted =>' . $i;
            }
        }
    }
    
    public function insertTopaz($array){

        $sql = "INSERT INTO TOPAZ_HOMOLOG (
                MODO,
                CRED_DEB,
                DT_PROC,
                DT_SEP,
                NSU,
                NSUTPZ,
                BANCO,
                AGENCIA,
                CONTA,
                VALOR,
                TARIFA,
                TPZPROC,
                TERMINAL,
                TERMINALID,
                STATUS,
                TIPO
            )VALUES(
                :MODO,
                :CRED_DEB,
                :DT_PROC,
                :DT_SEP,
                :NSU,
                :NSUTPZ,
                :BANCO,
                :AGENCIA,
                :CONTA,
                :VALOR,
                :TARIFA,
                :TPZPROC,
                :TERMINAL,
                :TERMINALID,
                :STATUS,
                :TIPO
            )";

        $add = oci_parse($this->ConnectionLocal(), $sql);

        $amount = sizeof($array);

        for ($i = 0; $i < $amount; $i++) {

            oci_bind_by_name($add, "MODO", $array[$i]['MODO']);
            oci_bind_by_name($add, "CRED_DEB", $array[$i]['CRED_DEB']);
            oci_bind_by_name($add, "DT_PROC", $array[$i]['DT_PROC']);
            oci_bind_by_name($add, "DT_SEP", $array[$i]['DT_SEP']);
            oci_bind_by_name($add, "NSU", $array[$i]['NSU']);
            oci_bind_by_name($add, "NSUTPZ", $array[$i]['NSUTPZ']);
            oci_bind_by_name($add, "BANCO", $array[$i]['BANCO']);
            oci_bind_by_name($add, "AGENCIA", $array[$i]['AGENCIA']);
            oci_bind_by_name($add, "CONTA", $array[$i]['CONTA']);
            oci_bind_by_name($add, "VALOR", $array[$i]['VALOR']);
            oci_bind_by_name($add, "TARIFA", $array[$i]['TARIFA']);
            oci_bind_by_name($add, "TPZPROC", $array[$i]['TPZPROC']);
            oci_bind_by_name($add, "TERMINAL", $array[$i]['TERMINAL']);
            oci_bind_by_name($add, "TERMINALID", $array[$i]['TERMINALID']);
            oci_bind_by_name($add, "STATUS", $array[$i]['STATUS']);
            oci_bind_by_name($add, "TIPO", $array[$i]['TIPO']);

            oci_execute($add);

            if ($add) {
                print '<h4>Inserted : ' . $i . ' => ' . $array[$i]['NSU'];
            }
        }
    }

    public function insertSaquePague($array){


        var_dump($array);

        exit();


        $sql = "INSERT INTO SAQUE_PAGUE_HOMOLOG (
                TP_B0,
                TT_B0,
                FT_B0,
                MOEDA_B0,
                CIF_B0,
                TIP_B0,
                CUSTODIA_B0,
                ORP_B0,
                IT_B1,
                NSU_B1,
                DTOP_B1,
                HP_B1,
                DTLANC_B1,
                STATUSOP_B1,
                CODOP_B1,
                VALOROP_B1,
                IDCLIENTE_B1,
                NSU_B2,
                DTOP_B2,
                HP_B2,
                CIF_B2,
                CAD_B2,
                NCD_B2,
                NSUAUT_B2
            )VALUES(
                :TP_B0,
                :TT_B0,
                :FT_B0,
                :MOEDA_B0,
                :CIF_B0,
                :TIP_B0,
                :CUSTODIA_B0,
                :ORP_B0,
                :IT_B1,
                :NSU_B1,
                :DTOP_B1,
                :HP_B1,
                :DTLANC_B1,
                :STATUSOP_B1,
                :CODOP_B1,
                :VALOROP_B1,
                :IDCLIENTE_B1,
                :NSU_B2,
                :DTOP_B2,
                :HP_B2,
                :CIF_B2,
                :CAD_B2,
                :NCD_B2,
                :NSUAUT_B2
            )";

        $amount = 5;
        $add = oci_parse($this->ConnectionOracleDesenv(), $sql);

        for ($i = 0; $i < $amount; $i++) {

            oci_bind_by_name($add, "TP_B0", $array['TP_B0'][$i]);
            oci_bind_by_name($add, "TT_B0", $array['TT_B0'][$i]);
            oci_bind_by_name($add, "FT_B0", $array['FT_B0'][$i]);
            oci_bind_by_name($add, "MOEDA_B0", $array['MOEDA_B0'][$i]);
            oci_bind_by_name($add, "CIF_B0", $array['CIF_B0'][$i]);
            oci_bind_by_name($add, "TIP_B0", $array['TIP_B0'][$i]);
            oci_bind_by_name($add, "CUSTODIA_B0", $array['CUSTODIA_B0'][$i]);
            oci_bind_by_name($add, "ORP_B0", $array['ORP_B0'][$i]);
            oci_bind_by_name($add, "IT_B1", $array['IT_B1'][$i]);
            oci_bind_by_name($add, "NSU_B1", $array['NSU_B1'][$i]);
            oci_bind_by_name($add, "DTOP_B1", $array['DTOP_B1'][$i]);
            oci_bind_by_name($add, "HP_B1", $array['HP_B1'][$i]);
            oci_bind_by_name($add, "DTLANC_B1", $array['DTLANC_B1'][$i]);
            oci_bind_by_name($add, "STATUSOP_B1", $array['STATUSOP_B1'][$i]);
            oci_bind_by_name($add, "CODOP_B1", $array['CODOP_B1'][$i]);
            oci_bind_by_name($add, "VALOROP_B1", $array['VALOROP_B1'][$i]);
            oci_bind_by_name($add, "IDCLIENTE_B1", $array['IDCLIENTE_B1'][$i]);
            oci_bind_by_name($add, "NSU_B2", $array['NSU_B2'][$i]);
            oci_bind_by_name($add, "DTOP_B2", $array['DTOP_B2'][$i]);
            oci_bind_by_name($add, "HP_B2", $array['HP_B2'][$i]);
            oci_bind_by_name($add, "CIF_B2", $array['CIF_B2'][$i]);
            oci_bind_by_name($add, "CAD_B2", $array['CAD_B2'][$i]);
            oci_bind_by_name($add, "NCD_B2", $array['NCD_B2'][$i]);
            oci_bind_by_name($add, "NSUAUT_B2", $array['NSUAUT_B2'][$i]);

            oci_execute($add);

            if ($add) {
                print '<h4>Inserted!';
                print '<br/>';
            }
        }
    }

    public function insertFileConciliation($array){

        $array = $array['ARQUIVO_CONCILIACAO'];
        
        $sql = "INSERT INTO ARQUIVOS_CONCILIACAO (
            NOME_ARQUIVO,
            CIF_A0,
            ISPP_A0,
            DTCONT_A0,
            TOTAL_REGISTROS_ZZ,
            TOTAL_CREDITOS_ZZ,
            TOTAL_DEBITOS_ZZ
           
        )VALUES(
            :NOME_ARQUIVO,
            :CIF_A0,
            :ISPP_A0,
            :DTCONT_A0,
            :TOTAL_REGISTROS_ZZ,
            :TOTAL_CREDITOS_ZZ,
            :TOTAL_DEBITOS_ZZ
            
        )";

        $add = oci_parse($this->Connection(), $sql);

            oci_bind_by_name($add, "NOME_ARQUIVO", $array['NOME_ARQUIVO']);
            oci_bind_by_name($add, "CIF_A0", $array['CIF_A0'][0]);
            oci_bind_by_name($add, "ISPP_A0", $array['ISPP_A0'][0]);
            oci_bind_by_name($add, "DTCONT_A0", $array['DTCONT_A0'][0]);
            oci_bind_by_name($add, "TOTAL_REGISTROS_ZZ", $array['TOTAL_REGISTROS_ZZ'][0]);
            oci_bind_by_name($add, "TOTAL_CREDITOS_ZZ", $array['TOTAL_CREDITOS_ZZ'][0]);
            oci_bind_by_name($add, "TOTAL_DEBITOS_ZZ", $array['TOTAL_DEBITOS_ZZ'][0]);

            oci_execute($add);

            // if ($add) {
            //     print 1;
            // }
        
    }

    public function insertServiceConciliation($array){
        
  
        $array = $array['SERVICO_CONCILIACAO'];
        // var_dump($array);
        // exit();
        $loop = $array['ID_ARQUIVO_CONCILIACAO']['LOOP'];
        $amount = sizeof($loop);

    
        $sql = "INSERT INTO SERVICOS_CONCILIACAO (
            ID_ARQUIVO_CONCILIACAO,
            DTREF_B0,
            TP_B0,
            TT_B0,
            FT_B0,
            MOEDA_B0,
            CIF_B0,
            TIP_B0,
            CUSTODIA_B0,
            ORP_B0,
            TOTAL_REG_TRANS_BZ,
            TOTAL_CRED_EFT_BZ,
            TOTAL_DEB_EFT_BZ,
            TOTAL_REG_OCO_BZ,
            TOTAL_TRANS_BZ,
            TOTAL_OCOR_BZ,
            TOTAL_CONTEST_BZ
           
        )VALUES(
            :ID_ARQUIVO_CONCILIACAO,
            :DTREF_B0,
            :TP_B0,
            :TT_B0,
            :FT_B0,
            :MOEDA_B0,
            :CIF_B0,
            :TIP_B0,
            :CUSTODIA_B0,
            :ORP_B0,
            :TOTAL_REG_TRANS_BZ,
            :TOTAL_CRED_EFT_BZ,
            :TOTAL_DEB_EFT_BZ,
            :TOTAL_REG_OCO_BZ,
            :TOTAL_TRANS_BZ,
            :TOTAL_OCOR_BZ,
            :TOTAL_CONTEST_BZ
        ) ";
        //RETURNING ID INTO :ID_SERVICO_CONCILIACAO
     
        $add = oci_parse($this->Connection(), $sql);
    

        for($i = 0; $i < $amount; $i++){
            
            oci_bind_by_name($add, "ID_ARQUIVO_CONCILIACAO", $array['DTREF_B0'][$i]);
            oci_bind_by_name($add, "DTREF_B0", $array['DTREF_B0'][$i]);
            oci_bind_by_name($add, "TP_B0", $array['TP_B0'][$i]);
            oci_bind_by_name($add, "TT_B0", $array['TT_B0'][$i]);
            oci_bind_by_name($add, "FT_B0", $array['FT_B0'][$i]);
            oci_bind_by_name($add, "MOEDA_B0", $array['MOEDA_B0'][$i]);
            oci_bind_by_name($add, "CIF_B0", $array['CIF_B0'][$i]);
            oci_bind_by_name($add, "TIP_B0", $array['TIP_B0'][$i]);
            oci_bind_by_name($add, "CUSTODIA_B0", $array['CUSTODIA_B0'][$i]);
            oci_bind_by_name($add, "ORP_B0", $array['ORP_B0'][$i]);
            oci_bind_by_name($add, "TOTAL_REG_TRANS_BZ", $array['TOTAL_REG_TRANS_BZ'][$i]);
            oci_bind_by_name($add, "TOTAL_CRED_EFT_BZ", $array['TOTAL_CRED_EFT_BZ'][$i]);
            oci_bind_by_name($add, "TOTAL_DEB_EFT_BZ", $array['TOTAL_DEB_EFT_BZ'][$i]);
            oci_bind_by_name($add, "TOTAL_REG_OCO_BZ", $array['TOTAL_REG_OCO_BZ'][$i]);
            oci_bind_by_name($add, "TOTAL_TRANS_BZ", $array['TOTAL_TRANS_BZ'][$i]);
            oci_bind_by_name($add, "TOTAL_OCOR_BZ", $array['TOTAL_OCOR_BZ'][$i]);
            oci_bind_by_name($add, "TOTAL_CONTEST_BZ", $array['TOTAL_CONTEST_BZ'][$i]);
            

            oci_execute($add);

            if ($add) { 
                // print 1;
            }
        }
    }

    public function insertTransitionsConciliation($array){
        
        
        $array = $array['TRANSACOES_CONCILIACAO'];

        $amount = sizeof($array['NSU_B1']['LOOP']);
        // var_dump($array);
        
        $sql = "INSERT INTO TRANSACOES_CONCILIACAO (
            ID_SERVICO_CONCILIACAO,
            IT_B1,
            NSU_B1,
            DTOP_B1,
            HP_B1,
            DTLANC_B1,
            STATUSOP_B1,
            CODOP_B1,
            VALOROP_B1,
            IDCLIENTE_B1,
            NSU_B2,
            DTOP_B2,
            HP_B2,
            CIF_B2,
            CAD_B2,
            NCD_B2,
            NSUAUT_B2
           
        )VALUES(
            :ID_SERVICO_CONCILIACAO,
            :IT_B1,
            :NSU_B1,
            :DTOP_B1,
            :HP_B1,
            :DTLANC_B1,
            :STATUSOP_B1,
            :CODOP_B1,
            :VALOROP_B1,
            :IDCLIENTE_B1,
            :NSU_B2,
            :DTOP_B2,
            :HP_B2,
            :CIF_B2,
            :CAD_B2,
            :NCD_B2,
            :NSUAUT_B2
            
        )";

       

        $add = oci_parse($this->Connection(), $sql);

        for($i = 0; $i < $amount; $i++){

            oci_bind_by_name($add, "ID_SERVICO_CONCILIACAO", $array['DTLANC_B1'][$i]);
            oci_bind_by_name($add, "IT_B1", $array['IT_B1'][$i]);
            oci_bind_by_name($add, "NSU_B1", $array['NSU_B1'][$i]);
            oci_bind_by_name($add, "DTOP_B1", $array['DTOP_B1'][$i]);
            oci_bind_by_name($add, "HP_B1", $array['HP_B1'][$i]);
            oci_bind_by_name($add, "DTLANC_B1", $array['DTLANC_B1'][$i]);
            oci_bind_by_name($add, "STATUSOP_B1", $array['STATUSOP_B1'][$i]);
            oci_bind_by_name($add, "CODOP_B1", $array['CODOP_B1'][$i]);
            oci_bind_by_name($add, "VALOROP_B1", $array['VALOROP_B1'][$i]);
            oci_bind_by_name($add, "IDCLIENTE_B1", $array['IDCLIENTE_B1'][$i]);
            oci_bind_by_name($add, "NSU_B2", $array['NSU_B2'][$i]);
            oci_bind_by_name($add, "DTOP_B2", $array['DTOP_B2'][$i]);
            oci_bind_by_name($add, "HP_B2", $array['HP_B2'][$i]);
            oci_bind_by_name($add, "CIF_B2", $array['CIF_B2'][$i]);
            oci_bind_by_name($add, "CAD_B2", $array['CAD_B2'][$i]);
            oci_bind_by_name($add, "NCD_B2", $array['NCD_B2'][$i]);
            oci_bind_by_name($add, "NSUAUT_B2", $array['NSUAUT_B2'][$i]);

            oci_execute($add);

        
        }

        if ($add) {
            print 1;
        }
        
    }
}
