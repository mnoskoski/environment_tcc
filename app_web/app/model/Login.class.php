<?php

class Login extends ConnectionORACLE
{

    public function getLogin(){
        
        if (isset($_POST['login']) and isset($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
        } else {
            $login = 'NULL';
            $password = 'NULL';
        }


        $array = array();
        $sql = ('SELECT ID, NAME, LOGIN, PASSWORD, PHOTO, ACTIVE FROM USERS_PESP WHERE LOGIN = :LOGIN AND PASSWORD = :PASSWORD AND ACTIVE = 1');
        $stId = oci_parse($this->Connection(), $sql);
        oci_bind_by_name($stId, ':LOGIN', $login);
        oci_bind_by_name($stId, ':PASSWORD', $password);
        oci_execute($stId);

        while (($row = oci_fetch_assoc($stId)) != false) {
            array_push($array, $row);
        }
        $count = oci_num_rows($stId);

        if ($count > 0) {

            SESSION_START();
            $_SESSION['ID'] = $array[0]['ID'];
            $_SESSION['NAME'] = $array[0]['NAME'];
            $_SESSION['LOGIN'] = $array[0]['LOGIN'];
            $_SESSION['PHOTO'] = $array[0]['PHOTO'];
            $_SESSION['PASSWORD'] = $array[0]['PASSWORD'];

            $auth = array(
                'success' => true
            );
            print json_encode($auth);
        } else {
            $auth = array(
                'success' => false
            );
            print json_encode($auth);
        }
    }
}
