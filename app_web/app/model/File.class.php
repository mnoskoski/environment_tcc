<?php

require_once './controllers/OperationsSaquePaque.controller.php';


class File extends OperationsSaquePaque{

    public $dir = './views/files/';
    public $dirAdd = './views/files/';
    public $amount = 100;
    public $dataSaquePague;
    public $totalSaquePague;



    public function __construct(){

        // $this->readFile();
    }

    public function nameFile($number){

        $scanDir = scandir($this->dir);
        foreach ($scanDir as $files) {
            if ($files != "." && $files != "..") {
                $nameFile[] = $files;
            }
        }

        return $nameFile[$number];

    }
    public function showFile(){
        $this->readFile();
        $totalValues = $this->getDataSaquePague();
        // var_dump($totalValues['ARQUIVO_CONCILIACAO']);
        // return $totalValues;
        return $totalValues['ARQUIVO_CONCILIACAO'];

    }
    public function readFile(){

        $i = 0;
      
        $dataSaquePague = array(
            //A0 ARRAY BIDIMENSIONAL
            'ARQUIVO_CONCILIACAO' => array(
                'NOME_ARQUIVO' => $this->nameFile(0),
                'CIF_A0' => array(),
                'ISPP_A0' => array(),
                'DTCONT_A0' => array(),
                'TOTAL_REGISTROS_ZZ' => array(),
                'TOTAL_CREDITOS_ZZ' => array(),
                'TOTAL_DEBITOS_ZZ' => array()

            ),
            'SERVICO_CONCILIACAO' => array(
                'ID_ARQUIVO_CONCILIACAO' => array(
                    $this->nameFile(0),
                    'LOOP' => array()
                ),
                //B0 ARRAY TRIDIMENSIONAL
                'DTREF_B0' => array(
                    'LOOP' => array()
                ),
                'TP_B0' => array(
                    'LOOP' => array()
                ),
                'TT_B0' => array(
                    'LOOP' => array()
                ),
                'FT_B0' => array(
                    'LOOP' => array()
                ),
                'MOEDA_B0' => array(
                    'LOOP' => array()
                ),
                'CIF_B0' => array(
                    'LOOP' => array()
                ),
                'TIP_B0' => array(
                    'LOOP' => array()
                ),
                'CUSTODIA_B0' => array(
                    'LOOP' => array()
                ),
                'ORP_B0' => array(
                    'LOOP' => array()
                ),
                'TOTAL_REG_TRANS_BZ' => array(
                    'LOOP' => array()
                ),
                'TOTAL_CRED_EFT_BZ' => array(
                    'LOOP' => array()
                ),
                'TOTAL_DEB_EFT_BZ' => array(
                    'LOOP' => array()
                ),
                'TOTAL_REG_OCO_BZ' => array(
                    'LOOP' => array()
                ),
                'TOTAL_TRANS_BZ' => array(
                    'LOOP' => array()
                ),
                'TOTAL_OCOR_BZ' => array(
                    'LOOP' => array()
                ),
                'TOTAL_CONTEST_BZ' => array(
                    'LOOP' => array()
                ),
                
            ),

            //B1 ARRAY BIDIMENSIONAL
            'TRANSACOES_CONCILIACAO' => array(
                'ID_SERVICO_CONCILIACAO' => array(
                  
                    'LOOP' => array()
                ),
                'IT_B1' => array(
                    'LOOP' => array()
                ),
                'NSU_B1' => array(
                    'LOOP' => array()
                ),
                'DTOP_B1' => array(
                    'LOOP' => array()
                ),
                'HP_B1' => array(
                    'LOOP' => array()
                ),
                'DTLANC_B1' => array(
                    'LOOP' => array()
                ),
                'STATUSOP_B1' => array(
                    'LOOP' => array()
                ),
                'CODOP_B1' => array(
                    'LOOP' => array()
                ),
                'VALOROP_B1' => array(
                    'LOOP' => array()
                ),
                'IDCLIENTE_B1' => array(
                    'LOOP' => array()
                ),
                //B2 ARRAY BIDIMENSIONAL
                'NSU_B2' => array(
                    'LOOP' => array()
                ),
                'DTOP_B2' => array(
                    'LOOP' => array()
                ),
                'HP_B2' => array(
                    'LOOP' => array()
                ),
                'CIF_B2' => array(
                    'LOOP' => array()
                ),
                'CAD_B2' => array(
                    'LOOP' => array()
                ),
                'NCD_B2' => array(
                    'LOOP' => array()
                ),
                'NSUAUT_B2' => array(
                    'LOOP' => array()
                )
            ),

        );


        if ($pointer = fopen($this->dirAdd.$this->nameFile(0), 'r')) {
            if ($pointer != false) {

                while (!feof($pointer)) {
                    $row = fgets($pointer);
                    $identificator = substr($row, 0, 2);

                    switch ($identificator) {

                        case 'A0':
                                // A0 DECODIFICADO 
                                // B5 CONTESTAÇÕES
                                // DEBITOS E CREDITOS
                            $unit = substr($row, 2, 2);
                            $CIFA0 = substr($row, 4, 4);
                            $ISPPA0 = substr($row, 8, 3);
                            $DTCNTA0 = substr($row, 16, 8);
                            $FILLERA0 = substr($row, 16, 57);
                            $NSUREGA0 = substr($row, 73, 6);
                            $IDEXTA0 = substr($row, 79, 1);
                            
                            array_push($dataSaquePague['ARQUIVO_CONCILIACAO']['CIF_A0'], $CIFA0);
                            array_push($dataSaquePague['ARQUIVO_CONCILIACAO']['ISPP_A0'], $ISPPA0);
                            array_push($dataSaquePague['ARQUIVO_CONCILIACAO']['DTCONT_A0'], $DTCNTA0);

                            break;

                        case 'B0':
                            // B0 DECODIFICADO 
                            $DTREF_B0 = substr($row, 2, 8);
                            $NSA = substr($row, 10, 6);
                            $NSP = substr($row, 16, 10);
                            $TP = substr($row, 26, 1); // IGNORAR O PRIMEIRO DIGITO
                            $TT = substr($row, 27, 1);
                            $FT = substr($row, 28, 1);
                            $coin = substr($row, 29, 3);
                            $CIF = substr($row, 32, 4);
                            $partner = substr($row, 36, 7);
                            $custody = substr($row, 43, 4);
                            $FILLERB0 = substr($row, 47, 26);
                            //ORIGEM DA OPERAÇÃO {referencia transições nacionais /  internacionais}
                            
                            $ORP_B0 = substr($row, 47, 1);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['ID_ARQUIVO_CONCILIACAO']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['DTREF_B0'], $DTREF_B0);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['DTREF_B0']['LOOP'], $i);
                            
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['ID_SERVICO_CONCILIACAO'],$i);
                            
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['TP_B0'], $TP);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['TP_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['TT_B0'], $TT);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['TT_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['FT_B0'], $FT);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['FT_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['MOEDA_B0'], $coin);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['MOEDA_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['CIF_B0'], $CIF);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['CIF_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['TIP_B0'], $TP);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['TIP_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['CUSTODIA_B0'], $custody);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['CUSTODIA_B0']['LOOP'], $i);

                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['ORP_B0'], $ORP_B0);
                            array_push($dataSaquePague['SERVICO_CONCILIACAO']['ORP_B0']['LOOP'], $i);
                            
                            $this->dataSaquePague = $dataSaquePague;
                            
                            break;

                        case 'B1':
                            // B1 DECODIFICADO 

                            $IT = substr($row, 2, 8);
                            $NSU = substr($row, 10, 12);
                            $DO = substr($row, 22, 4);
                            $HO = substr($row, 26, 4);
                            $DL = substr($row, 30, 8);
                            $SO = substr($row, 38, 1);
                            $CO = substr($row, 39, 6);
                            $VO = substr($row, 45, 8);
                            $IC = substr($row, 53, 18);
                            $MOB1 = substr($row, 71, 1);
                            $FILLERB1 = substr($row, 72, 1);
                            $NSUREGB1 = substr($row, 73, 6);
                            $IDEXTB1 = substr($row, 79, 1);

                        
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['IT_B1'], $IT);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['IT_B1']['LOOP'], $i);
                            
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['NSU_B1'], $NSU);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['NSU_B1']['LOOP'], $i);

                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['DTOP_B1'], $DO);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['DTOP_B1']['LOOP'], $i);
                            
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['HP_B1'], $HO);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['HP_B1']['LOOP'], $i);
                            
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['DTLANC_B1'], $DL);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['DTLANC_B1']['LOOP'], $i);

                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['STATUSOP_B1'], $SO);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['STATUSOP_B1']['LOOP'], $i);

                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['CODOP_B1'], $CO);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['CODOP_B1']['LOOP'], $i);

                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['VALOROP_B1'], $VO);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['VALOROP_B1']['LOOP'], $i);

                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['IDCLIENTE_B1'], $IC);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['IDCLIENTE_B1']['LOOP'], $i);

                            $this->dataSaquePague = $dataSaquePague;
                            // 910800 => Recarga Pré-pago
                            // 302000 => Saque CC

                            // if($SO == 3){
                            //     print '<h4>ID:'.$i.' | NSU => '.$NSU.' | CODOP => '.$CO.' | STATUS OP =>'.$SO.' | R$ '.$this->numberFormat($VO);
                            // }

                            // print '<br/>';

                            break;

                        case 'B2':

                            // B2 DECODIFICADO
                            $ITB2 = substr($row, 2, 8);
                            $TM = substr($row, 10, 1);
                            $NSUOP = substr($row, 11, 12);
                            $DOB2 = substr($row, 23, 4);
                            $HOB2 = substr($row, 27, 4);
                            $CIFD = substr($row, 31, 3);
                            $ISPPB2 = substr($row, 34, 3);
                            $CAD = substr($row, 37, 5);
                            $NCD = substr($row, 42, 10);
                            $IDTB2 = substr($row, 52, 1);
                            $MOB2 = substr($row, 53, 1);
                            $NSUA = substr($row, 54, 12);
                            $CODORIG = substr($row, 66, 3);
                            $FILLERB2 = substr($row, 69, 4);
                            $NSUREGB2 = substr($row, 73, 6);
                            $IDEXT = substr($row, 79, 1);
                            
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['NSU_B2'], $NSUOP);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['DTOP_B2'], $DOB2);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['HP_B2'], $HOB2);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['CIF_B2'], $CIFD);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['CAD_B2'], $CAD);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['NCD_B2'], $NCD);
                            array_push($dataSaquePague['TRANSACOES_CONCILIACAO']['NSUAUT_B2'], $NSUA);

                            $this->dataSaquePague = $dataSaquePague;


                            break;

                            // case 'B5':
                            //     //DECODIFICAR

                            // break;

                        case 'BZ':
                                // BZ DECODIFICADO
                                $NSABZ = substr($row, 2, 6);
                                $NSPBZ = substr($row, 8, 10);
                                $TRTBZ = substr($row, 18, 6);
                                $TCEBZ = substr($row, 24, 14);
                                $TDEBZ = substr($row, 38, 14);
                                $TROBZ = substr($row, 52, 6);
                                $TTBZ = substr($row, 58, 6);
                                $TOBZ = substr($row, 64, 6);
                                $TCBZ = substr($row, 70, 3);
                                $NSUREGBZ = substr($row, 73, 6);
                                $IDEXTBZ = substr($row, 79, 1);

                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_REG_TRANS_BZ'], $TRTBZ);
                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_CRED_EFT_BZ'], $this->numberFormat($TCEBZ));
                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_DEB_EFT_BZ'], $this->numberFormat($TDEBZ));
                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_REG_OCO_BZ'], $TROBZ);
                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_TRANS_BZ'], $TTBZ);
                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_OCOR_BZ'], $TOBZ);
                                array_push($dataSaquePague['SERVICO_CONCILIACAO']['TOTAL_CONTEST_BZ'], $TCBZ);


                        break;

                        case 'ZZ':
                            // ZZ DECODIFICADO 
                            $UCZZ = substr($row, 2, 2);
                            $CIFZZ = substr($row, 4, 5);
                            $ISPPZZ = substr($row, 8, 3);
                            $TGRZZ = substr($row, 11, 8);
                            $TGCZZ = substr($row, 19, 15);
                            $TGDZZ = substr($row, 34, 15);
                            $FILLERZZ = substr($row, 49, 24);
                            $NSUREGZZ = substr($row, 73, 6);
                            $IDEXTZZ = substr($row, 79, 1);
                    
                            array_push($dataSaquePague['ARQUIVO_CONCILIACAO']['TOTAL_REGISTROS_ZZ'], $TGRZZ);
                            array_push($dataSaquePague['ARQUIVO_CONCILIACAO']['TOTAL_CREDITOS_ZZ'],  $this->numberFormat($TGCZZ));
                            array_push($dataSaquePague['ARQUIVO_CONCILIACAO']['TOTAL_DEBITOS_ZZ'],  $this->numberFormat($TGDZZ));

                            $this->dataSaquePague = $dataSaquePague;
                           
                            break;


                    }

                    $i++;
                }
                fclose($pointer);
            } else {
                echo "<p> It was not possible to open the File!</p>";
            }
        } else {
            print "Error!";
        }
    }
    public function getDataSaquePague(){

        return $this->dataSaquePague;
    }

    public function getTotalSaquePague(){

        return $this->totalSaquePague;
    }
}
