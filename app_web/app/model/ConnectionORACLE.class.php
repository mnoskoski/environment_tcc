<?php

// $_ENV = parse_ini_file('/var/www/html/.env');
// $_ENV = parse_ini_file('views/app/.env');

class ConnectionORACLE{

  private $user = 'topazio';
  private $password = '3UR0P435';
  private $connection_string =
  '
    (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = 10.100.98.8)(PORT = 1522))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (sid = hcritico)
        )
    )
  ';
  public function Connection(){
 
    //$conn = oci_connect($_ENV['DB_USER'], $_ENV['DB_PASSWORD'], $_ENV['DB_DSN']);
     $conn = oci_connect($this->user, $this->password, $this->connection_string);

    if (!$conn) {
      $e = oci_error();
      trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    } else {
      // print 'Connected!';
      // var_dump($_ENV);
      return $conn;
    }
  }
}
