<?php

class Session extends ConnectionORACLE
{

    function __construct()
    {
        SESSION_START();

        if (isset($_SESSION['LOGIN']) AND isset($_SESSION['PASSWORD'])) {

            $sql = ('SELECT LOGIN, PASSWORD, ACTIVE FROM USERS_PESP WHERE LOGIN = :LOGIN AND PASSWORD = :PASSWORD AND ACTIVE = 1');
            $stId = oci_parse($this->Connection(), $sql);
            oci_bind_by_name($stId, ':LOGIN', $_SESSION['LOGIN']);
            oci_bind_by_name($stId, ':PASSWORD', $_SESSION['PASSWORD']);
            oci_execute($stId);

            $count = oci_num_rows($stId);

            if($count < 0){
                // alert("Logue-se no sistema!!")
                $this->logof();
            }

        } else {
            $this->logof();
        }
    }
    public function logof(){
        SESSION_START();
        SESSION_DESTROY();
        SESSION_UNSET();
        print '
                <script>
                    location.href="/login/"
                </script>
            ';
        exit();
    }
}

?>